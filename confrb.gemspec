Gem::Specification.new do |s|
  s.name        = 'confrb'
  s.version     = '2.3'
  s.date        = '2020-02-07'
  s.summary     = 'Plaintext configuration library for Ruby'
  s.description = 'A Ruby Gem which allows you to store configuration files using hidden directories. Supports JSON, YAML, and plaintext. Data is unencrypted, but you can hash/encrypt data yourself before passing it to be written.  Useful for simple text adventure games, etc. Docs and examples at https://gitlab.com/Gsbhasin84/confrb'
  s.author     = 'Gurjus Bhasin'
  s.email       = 'gsbhasin84@gmail.com'
  s.files       = ['lib/confrb.rb']
  s.homepage  = 'https://gitlab.com/Gsbhasin84/confrb'
  s.license = 'MIT'
end
