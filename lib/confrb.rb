# Require everything!
require "fileutils"
require "json"
require "yaml"
##
# Main class for Conf.rb
# @author gsbhasin84
class Confrb
  ##
  # Auto-create config storage directory, prepending '.' to
  # given name. Use .new() on this class to run this! Can create subdirs recursively, but only
  # root directory gets '.' prepended
  # @param [String] db The name of the configuration directory to create. 
  def initialize(db)
    @db = db
    dotname = "." + @db
    FileUtils.mkdir_p(dotname)
  end

  ##
  # Access the given database, prepending '.' to given name. Raises an exception if nonexistent.
  # @param [String] db The name of the configuration directory to access
  def self.access(db)
    if Dir.exist? '.' + db
      Confrb.new db
    else
      raise "Directory does not exist!"
    end
  end

  ##
  # Create/overwrite a config file and write data to it. Can
  # serialize data as yaml or json, or write a string.
  # @param [String] cfgname Name of config file to write to 
  # @param [String] content The content to write, defaults to an empty string.
  # @param [Hash] opts Optional param hash
  # @option opts [boolean] :json If true, serialize content
  #   as JSON  Default: false
  # @option opts [boolean] :yaml If true, serialize content
  #   as YAML Default: false (json: true takes precedence)
  # @option opts [boolean] :newline If true, prepends a newline to the beginning of the content. Default: false
  # @return path to configuration file created.
  def mkcfg(cfgname, content='', opts = {})
    as_json = opts.fetch(:json, false) 
    as_yaml = opts.fetch(:yaml, false)
    pnewline = opts.fetch(:prepend_newline, false)
    anewline = opts.fetch(:append_newline, false)
    if pnewline == true
      content = "\n" + content
    elsif anewline == true
      content = content + "\n"
    end
    dotdir = "." + @db
    cfgpath = File.join(dotdir, cfgname)
    FileUtils.touch(cfgpath)
    if as_json == true
      content = content.to_json
      File.write(cfgpath, content)
    elsif as_yaml == true
      content = content.to_yaml
      File.write(cfgpath, content)
    else
      File.write(cfgpath, content)
    end
  end

  ##
  # Creates nested dir(s) inside a database.
  # @param [String] nesteddir Nested dir(s) to create in dir
  #   Make sure path delimiter is OS correct, or use
  #   File.join
  # @return [String] path to nested directory created.
  def mknested(nesteddir)
    dotdir = "." + @db
    FileUtils.mkdir_p(File.join(dotdir, nesteddir))
  end

  ##
  # Read a config file from disk
  # @param [String] cfgname Config file name to read
  # @param [Hash] opts Optional param hash
  # @option opts [boolean] :json If true, deserialize
  #   content as JSON (Default false)
  # @option opts [boolean] :yaml If true, deserialize
  #   content as YAML. Default false (json: true takes
  #   precedence
  # @return [String|Hash] Loaded contents, either a String
  #   if not indicated to treat as JSON or YAML, or a data structure
  #   deserialized from content string
  def readcfg(cfgname, opts = {})
    dotdir = "." + @db
    as_json = opts.fetch(:json, false)
    as_yaml = opts.fetch(:yaml, false)
    if as_json == true
      content = JSON::load(File.read(File.join(dotdir, cfgname)))
      return content
    elsif as_yaml == true
      content = YAML::load(File.read(File.join(dotdir, cfgname)))
      return content
    else
      File.read(File.join(dotdir, cfgname))
    end
  end

  ##
  # Remove a configuration file
  # @param [String] cfgname Configuration file name, use nested directories here with File.join()
  # @return [String] path to file deleted.
  def rmcfg(cfgname)
    dotdir = "." + @db
    File.delete(File.join(dotdir, cfgname))
  end

  ##
  # Remove a whole database
  # @return [String] Path to deleted database
  def rmdb()
    dotdir = "." + @db
    FileUtils.rm_rf(dotdir)
  end

  ## Remove a nested directory
  # @param [String] Nested directory path, please use File.join() if it's more than one layer deep.
  # @return [String] Path to deleted nested directory
  def rmnested(nested)
    dotdir = File.join("." + @db, nested)
    FileUtils.rm_rf(dotdir)
  end

  ##
  # Create/append to a config file and write data to it. Can
  # serialize data as yaml or json, or write a string. Writes a newline at the beginning.
  # @param [String] cfgname Name of config file to write to
  # @param [String] content The content to write, defaults to an empty string.
  # @param [Hash] opts Optional param hash
  # @option opts [boolean] :json If true, serialize content
  #   as JSON  Default: false
  # @option opts [boolean] :yaml If true, serialize content
  #   as YAML Default: false (json: true takes precedence)
  # @option opts [boolean] :newline If true, prepend a newline to content. Default: true
  # @return path to configuration file created.
  def writecfg(cfgname, content, opts = {})
    as_json = opts.fetch(:json, false)
    as_yaml = opts.fetch(:yaml, false) 
    pnewline = opts.fetch(:prepend_newline, true)
    anewline = opts.fetch(:append_newline, false)
    dotdir = "." + @db
    cfgpath = File.join(dotdir, cfgname)
    FileUtils.touch(cfgpath)
    if pnewline == true
      content = "\n" + content
    elsif anewline == true
      content = content + "\n"
    end
    if as_json == true
      content = content.to_json
      File.write(cfgpath, content, mode: "a")
    elsif as_yaml == true
      content = content.to_yaml
      File.write(cfgpath, content, mode: "a")
    else
      File.write(cfgpath, content, mode: "a")
    end
  end
  ##
  # Check if a file exists.
  # @param [String] filename the filename to check existence of
  # @return Boolean showing if file exists or not. 
  def exist?(filename)
    return File.exist?(File.join(@db + ".", filename))
  end
end
