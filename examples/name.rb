require 'confrb' # Require Conf.rb
db = Confrb.new 'nametest' # Create the new Conf.rb nametest database
begin # Try to do the following: 
    db = Confrb.access "nametest" # This will throw an error if doesn't exist, and will continue to rescue
    name = db.readcfg 'name.cfg'
    puts "Hey, #{name}! I got your name from the configuration file!"
rescue # If the above fails, does the following: 
    db = Confrb.new "nametest"
    print "What's your name? Type it: "
    name = gets.chomp
    db.mkcfg 'name.cfg', name
    puts "Hey, #{name}! I got your name from user input!"
end