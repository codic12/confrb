# Conf.rb
Conf.rb is a pleasant library for Ruby that allows for making configuration files.
# Contributing
Feel free to make merge requests, issues, whatever your heart pleases! 
## Confrb - class
Note; instead of using the `new` class method to make a new instance, you can do `Confrb.access(arg);`. This throws an error if the database doesn't exist; useful for checking if it does (with the `begin..rescue` construct). Also, use `File.join()` to join paths; works 100%.

```ruby
mkcfg('cfgname', 'content', as_json: false, as_yaml: false, prepend_newline: false, append_newline: false)
```
Creates a new config file `cfgname` with content `content`. The content defaults to an empty string.
If file exists, it is overwritten
Options in the options object literal are self explanatory.
```ruby
mknested('nesteddirname')
```
This creates a nested directory `nesteddirname` inside your database.

```ruby
readcfg('filename', as_json: false, as_yaml: false, prepend_newline: false, append_newline: false)
```
Reads a config file `filename`.
Options in the options object literal are self explanatory.

```ruby
rmcfg('filename')
```
Removes the configuration file `filename` in your database.
```ruby
rmdb()
```
Simply removes the whole database that this is called on. 

```ruby
rmnested('nesteddirname')
```
Removes a nested directory `nesteddirname` inside your database.

```ruby
writecfg('cfgname', 'content', as_json: false, as_yaml: false, prepend_newline: true, append_newline: false)
```
Creates a new config file `cfgname` with content `content`. The content defaults to an empty string.
If file already exists, it is appended to. Please use mkcfg to make new files.

Options in the options object literal are self explanatory.

```ruby
exist?('filename')
```
Returns a boolean (true/false) based on if the file `filename` exists in your database or not.
